/**
 * @author Jay Urbain
 *
 * @version 12/28/2013, 9/2/2014
 */

package deidentification;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.List;

import deidentification.NamedEntityRecognition;
import util.SimpleIRUtilities;

/**
 * @author jayurbain
 *
 * Processing thread for executing the de-identification process.
 * Takes NamedEntityRecogition class at construction.
 * Processes set of MedicalRecordWrapper set in List<MedicalRecordWrapper> recordList
 */
public class DeIdentificationThread extends Thread {
	
	NamedEntityRecognition namedEntityRecognition;
	List<MedicalRecordWrapper> recordList;

	/**
	 * @param namedEntityRecognition Named entity recognition class
	 */
	public DeIdentificationThread(
			NamedEntityRecognition namedEntityRecognition) {
		super();
		this.namedEntityRecognition = namedEntityRecognition;
		this.recordList = new ArrayList<MedicalRecordWrapper>();
	}

	/**
	 * Thread processing. Read records set in recordList, 
	 * execute namedEntityRecognition.performAnnotation on record.
	 */
	@Override
	public void run() {
		ThreadMXBean mxBean = ManagementFactory.getThreadMXBean();
		for( MedicalRecordWrapper r : recordList ) {
			long currentCPU1 = (mxBean.isThreadCpuTimeSupported())? mxBean.getThreadCpuTime(Thread.currentThread().getId()) : 0;
			r.setDeIdText( namedEntityRecognition.performAnnotation( r.getRegexText() ) );
			long currentCPU2 = (mxBean.isThreadCpuTimeSupported())? mxBean.getThreadCpuTime(Thread.currentThread().getId()) : (long)-1e6;
			long milliSeconds = (long) ((currentCPU2 - currentCPU1) / 1e6);
			r.setMillisecondsNER(milliSeconds);
			DeIdentification.EntityRecTotalTime = DeIdentification.EntityRecTotalTime + milliSeconds;
		}
	}
	
	
	/**
	 * @ return NamedEntityRecognition
	 */
	public NamedEntityRecognition getNamedEntityRecognition() {
		return namedEntityRecognition;
	}

	/**
	 * @param getNamedEntityRecognition 
	 */
	public void setNamedEntityRecognition(
			NamedEntityRecognition namedEntityRecognition) {
		this.namedEntityRecognition = namedEntityRecognition;
	}

	/**
	 * @ return List<MedicalRecordWrapper>
	 */
	public List<MedicalRecordWrapper> getRecordList() {
		return recordList;
	}

	/**
	 * @param List<MedicalRecordWrapper> 
	 */
	public void setRecordList(List<MedicalRecordWrapper> recordList) {
		this.recordList = recordList;
	}
}
