package util;

import java.io.IOException;

public class FormatRestore {
	
	
	public FormatRestore(){
	}
	
	
	public static String preProcessText(String s){
		
		
		// Run same deidentification.mcw.DeidentificationRegexMCW.preProcessText on the string except for new lines
		s = s.replaceAll("(\\r\\n|\\r|\\n)", " **LineBreak** ");
		s=s+" **LineBreak** ";
		s = s.replaceAll("\"", " ");
		s = s.replaceAll("#", " # "); 
		s = s.replaceAll("\\.", ". ");
		s = s.replaceAll(":", " : ");
		s = s.replaceAll(",", " , ");
		s = s.replaceAll(";", " ; ");
		s = s.replaceAll("_", " ");
		s = s.replaceAll("[\\t\\x0b\\f]", " ").trim();
		s = deidentification.mcw.DeidentificationRegexMCW.procedureAllCaps(s);

		return s;
	}
	
	
	/**
	 * restore line breaks in the deid string
	 */
	public String RestoreLines(String origtxt, String deidtxt) {
		
		// preprocess the deid and original text for easy comparison
		deidtxt = deidtxt.trim();
		origtxt  = preProcessText(origtxt);
		String[] origtxtArr = origtxt.split("\\s+");
		String[] deidtxtArr = deidtxt.split("\\s+");
		
		// compare original and deid text, find missing line breaks
		int posDeidtxt = 0;
		String deidtxtWithLines = "";
		
		
		if (deidtxtArr.length >2){
			deidtxtWithLines= deidtxtArr[0] + " " + deidtxtArr[1] + " ";
			posDeidtxt = 2;
		}
		
		try {
		for (int i = 2; i < origtxtArr.length-3; i++) {
			if (origtxtArr[i].equals("**LineBreak**")){
				for (int j = posDeidtxt; j < deidtxtArr.length-3; j++) {
					
					//handles: [wj-1] [wj] [**line break **] [wj+1]
					if (deidtxtArr[j].equals(origtxtArr[i-1]) && deidtxtArr[j+1].equals(origtxtArr[i+1])) {
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
					}
					//handles: [wj-1] [wj = [XXXX]] [**line break **] [wj+1]
					else if (deidtxtArr[j].startsWith("[") &&  deidtxtArr[j].endsWith("]") && deidtxtArr[j-1].equals(origtxtArr[i-2]) && deidtxtArr[j+1].equals(origtxtArr[i+1])) {
						
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
					}
					//handles: [wj-1 ] [wj ] [**line break **] [wj+1 = [XXXX]]
					else if (deidtxtArr[j+1].startsWith("[") &&  deidtxtArr[j+1].endsWith("]") && deidtxtArr[j].equals(origtxtArr[i-1]) && deidtxtArr[j+2].equals(origtxtArr[i+2])) {
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
					}
					//handles: [wj-1 = [XXXX]] [wj = [XXXX]] [**line break **] [wj+1]
					else if (deidtxtArr[j].startsWith("[") &&  deidtxtArr[j].endsWith("]") && deidtxtArr[j-1].startsWith("[") &&  deidtxtArr[j-1].endsWith("]") && deidtxtArr[j-2].equals(origtxtArr[i-3]) && deidtxtArr[j+1].equals(origtxtArr[i+1])) {
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
				}	
					//handles: [wj-1 ] [wj = [XXXX]] [**line break **] [wj+1= [XXXX]]
					else if (deidtxtArr[j].startsWith("[") &&  deidtxtArr[j].endsWith("]") && deidtxtArr[j+1].startsWith("[") &&  deidtxtArr[j+1].endsWith("]") && deidtxtArr[j-1].equals(origtxtArr[i-2]) && deidtxtArr[j+2].equals(origtxtArr[i+2])) {
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
				}	
					//handles: [wj-1 ] [wj] [**line break **] [wj+1= [XXXX]] [wj+2= [XXXX]]
					else if (deidtxtArr[j+2].startsWith("[") &&  deidtxtArr[j+2].endsWith("]") && deidtxtArr[j+1].startsWith("[") &&  deidtxtArr[j+1].endsWith("]") && deidtxtArr[j].equals(origtxtArr[i-1]) && deidtxtArr[j+3].equals(origtxtArr[i+3])) {
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
				}
					//handles: [wj-1 = [XXXX]] [wj = [XXXX]] [**line break **] [wj+1 [XXXX] ]
					else if (deidtxtArr[j].startsWith("[") &&  deidtxtArr[j].endsWith("]") && deidtxtArr[j-1].startsWith("[") &&  deidtxtArr[j-1].endsWith("]") && deidtxtArr[j+1].startsWith("[") &&  deidtxtArr[j+1].endsWith("]")  && deidtxtArr[j-2].equals(origtxtArr[i-3]) && deidtxtArr[j+2].equals(origtxtArr[i+2])) {
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
				}	
					//handles:  [wj-1  ] [wj = [XXXX]] [**line break **] [wj+1 = [XXXX] ]  [wj+2 = [XXXX] ]
					else if (deidtxtArr[j].startsWith("[") &&  deidtxtArr[j].endsWith("]") && deidtxtArr[j+1].startsWith("[") &&  deidtxtArr[j+1].endsWith("]") && deidtxtArr[j+2].startsWith("[") &&  deidtxtArr[j+2].endsWith("]")  && deidtxtArr[j-1].equals(origtxtArr[i-2]) && deidtxtArr[j+3].equals(origtxtArr[i+3])) {
						for (int k = posDeidtxt; k < j; k++) {
							deidtxtWithLines = deidtxtWithLines + deidtxtArr[k] + " ";
						}					
						deidtxtWithLines = deidtxtWithLines + deidtxtArr[j]+ System.lineSeparator() ;
						posDeidtxt = j+1;
						break;
				}	
					
									
				}
				
				}

		}
		}
		catch(Exception e) {
			//System.out.println("Could not restore the line breaks");
		}
			for (int j = posDeidtxt; j < deidtxtArr.length; j++) {
				deidtxtWithLines = deidtxtWithLines + deidtxtArr[j] + " ";
			}

		
		return deidtxtWithLines;
    }
	
//public static void main(String[] args) throws IOException {
//
//
//	    
//		String origtxt = "";		
//		String deidtxt ="";
//
//		FormatRestore frmt = new FormatRestore();
//		String output = frmt.RestoreLines(origtxt,deidtxt);
//		System.out.println(output);
//	}
}
