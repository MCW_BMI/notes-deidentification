package util;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class MsgLog {
	
	private final int RecordsThreshold = 5000;	//output status for every OutputNumRows
	private static PrintWriter logFile;
	
	public MsgLog(String fileName){
		// initialize Log file
		try {
			logFile = new PrintWriter(fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getRecordsThreshold() {
		return RecordsThreshold;
	}
	
	/**
	 * @param startDate - time when the DEID started
	 * @param Currentrow - the number of DEID records processed so far
	 * @param logFile - PrintWriter object to write log file
	 * print the status of DEID
	 */
    public void updateStatus(java.util.Date startDate,java.util.Date dbFinishDate, int Currentrow, double RegexTotalTime, double EntityRecTotalTime, boolean Finished) throws IOException{
    	
    	if (!Finished){
    		System.out.println("*************** Update *****************");
    		
    	} else {
    		System.out.println("*************** Finished *****************");
    		System.out.println("Summary:");
    	}
    	java.util.Date currentDate = new java.util.Date();
    	long CurrentTime = SimpleIRUtilities.getElapsedTimeSeconds(startDate, currentDate);
    	long DeidTime = SimpleIRUtilities.getElapsedTimeSeconds(dbFinishDate, currentDate);
    	long DBTime = SimpleIRUtilities.getElapsedTimeSeconds(startDate, dbFinishDate);

    	double RecordsPerSecond = 0;
    	if (Double.valueOf(CurrentTime) != 0 )
    		RecordsPerSecond = Currentrow/Double.valueOf(DeidTime);
    	else
    		RecordsPerSecond = Currentrow;	// to avoid division over zero

    	
    	String str1 = "Total time: "+CurrentTime+ " seconds";
    	String str2 = "Reading data from database: "+DBTime +" seconds" ;
    	String str3 = "Number of records processed: "+Currentrow;
    	String str4 = "Processing Rate (records/second): "+  RecordsPerSecond;
    	String str5 = "Total time for RegEx: "+ RegexTotalTime/1000.0+" seconds";
    	String str6 = "Total time for EntityRec: "+ EntityRecTotalTime/1000.0+" seconds";
    	String str7 = "Total time for Deid: "+ DeidTime+" seconds";

    	// progress output 
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
        System.out.println(str4);
        System.out.println(str5);
        System.out.println(str6);
        System.out.println(str7);

        // write to file
        logFile.println(str1);
        logFile.println(str2);
        logFile.println(str3);
        logFile.println(str4);
        logFile.println(str5);
        logFile.println(str6);
        logFile.println(str7);

        logFile.println();
        logFile.flush();
		
        if (Finished){
        	logFile.close();
		}
    }
}
