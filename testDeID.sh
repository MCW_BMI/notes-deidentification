#!/bin/sh
# Test Wrapper to test the example data loaded into the TEST_TABLE 
# will create the table TEST_TABLE_OUT so please don't name it the same as the table your selecting from in the "-query" line 
# 1/20/2015 - George Kowalski


java -Xms8192M -Xmx8192M -jar DeIdentificationX2.jar \
	-dburl "jdbc:mysql://localhost:3306"  \
	-login "idsc" \
	-password "***REMOVED***" \
	-dbname "IDSC" \
	-dbdriver "com.mysql.jdbc.Driver" \
	-query "select ID as id, ID as note_id, NOTE_TEXT as note_text, DATE_OFF as date_off from TEST_TABLE order by id" \
	-nthreads 5 \
	-recordsperthread 100 \
	-deidnotestablename "TEST_TABLE_OUT" \
	-namedentityrecognitionclass "deidentification.mcw.NamedEntityRecognitionMCW" \
	-regexdeidentificationclass "deidentification.mcw.DeidentificationRegexMCW" \
	-whitelistfilename "DeIdentificationX2/whitelist.txt" \
	-blacklistfilename "DeIdentificationX2/blacklist.txt" 
